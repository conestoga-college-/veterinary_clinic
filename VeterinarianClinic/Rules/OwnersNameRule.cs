﻿using System.Globalization;
using System.Windows.Controls;

namespace VeterinarianClinic
{
    public class OwnersNameRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            MyAnimal animal = new MyAnimal();
            if (value == null || !animal.ValidateOwnersName(value.ToString()))
            {
                return new ValidationResult(false, animal.StringOfOwnersNameRule());
            }

            return ValidationResult.ValidResult;
        }
    }
}
