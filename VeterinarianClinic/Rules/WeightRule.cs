﻿using System.Globalization;
using System.Windows.Controls;

namespace VeterinarianClinic
{
    public class WeightRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            MyAnimal animal = new MyAnimal();
            float weightInKilograms = 0;
            if (float.TryParse((string)value, out weightInKilograms))
            {
                if (!animal.ValidateWeight(weightInKilograms))
                {
                    return new ValidationResult(false, animal.StringOfWeightRule());
                }
            }
            return ValidationResult.ValidResult;
        }
    }
}
