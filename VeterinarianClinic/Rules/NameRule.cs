﻿using System.Globalization;
using System.Windows.Controls;

namespace VeterinarianClinic
{
    public class NameRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            MyAnimal animal = new MyAnimal();
            if (value == null || !animal.ValidateName(value.ToString()))
            {
                return new ValidationResult(false, animal.StringOfNameRule());
            }

            return ValidationResult.ValidResult;
        }
    }
}
