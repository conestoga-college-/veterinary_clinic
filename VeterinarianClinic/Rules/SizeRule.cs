﻿using System.Globalization;
using System.Windows.Controls;

namespace VeterinarianClinic
{
    public class SizeRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            MyAnimal animal = new MyAnimal();
            float sizeInCentimetres = 0;
            if (float.TryParse((string)value, out sizeInCentimetres))
            {
                if (!animal.ValidateSize(sizeInCentimetres))
                {
                    return new ValidationResult(false, animal.StringOfSizeRule());
                }
            }
            return ValidationResult.ValidResult;
        }
    }
}
