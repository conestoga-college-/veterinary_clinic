﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinarianClinic
{
    public class Horse : Animal
    {
        private bool isDomesticated;

        public bool IsDomesticated { get => isDomesticated; set => isDomesticated = value; }

        public Horse()
        {

        }
        public Horse(string name, string owner, float size, float weight, bool isDomesticated) : base(name, owner, size, weight)
        {
            this.isDomesticated = isDomesticated;
        }
    }
}
