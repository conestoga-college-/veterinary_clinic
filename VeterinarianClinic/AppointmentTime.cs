﻿using System;
using System.Collections.Generic;

namespace VeterinarianClinic
{
    public enum AppointmentTime
    {
        One = 900,
        Two = 930,
        Three = 1000,
        Four = 1030,
        Five = 1100,
        Six = 1400,
        Seven = 1430,
        Eight = 1500,
        Nine = 1530,
        Ten = 1600
    }

    public class AppointmentTimeString
    {
        public AppointmentTimeString()
        {

        }

        public List<string> GetAppointmentsStrings()
        {
            Array appointments = Enum.GetValues(typeof(AppointmentTime));
            List<string> result = new List<string>();

            for (int i = 0; i < appointments.Length; i++)
            {
                int convertedAppointment = (int)appointments.GetValue(i);
                result.Add(GetTimeString(convertedAppointment));
            }

            return result;
        }

        public string GetOneAppointmentString(int appointmentTimeId)
        {
            Array appointments = Enum.GetValues(typeof(AppointmentTime));
            int convertedAppointment = (int)appointments.GetValue(appointmentTimeId);
            return GetTimeString(convertedAppointment);
        }

        private string GetTimeString(int convertedAppointment)
        {
            return convertedAppointment.ToString().PadLeft(4, '0').Insert(2, ":");
        }
    }
}
