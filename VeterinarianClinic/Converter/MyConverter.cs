﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace VeterinarianClinic.Converter
{
    [ValueConversion(typeof(int), typeof(Brush))]
    class MyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            float i = float.Parse(value.ToString());
            if (i < 100)
            {
                return Brushes.White;
            }
            else
            {
                return Brushes.Coral;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
