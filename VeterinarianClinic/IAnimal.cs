﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinarianClinic
{
    public interface IAnimal
    {
        string Name { get; set; }
        string OwnersName { get; set; }
        float Size { get; set; }
        float Weight { get; set; }
        bool ValidateName(string value);
        string StringOfNameRule();

        bool ValidateOwnersName(string value);


        string StringOfOwnersNameRule();

        bool ValidateSize(float value);

        string StringOfSizeRule();

        string StringOfWeightRule();
        bool ValidateWeight(float value);
    }
}
