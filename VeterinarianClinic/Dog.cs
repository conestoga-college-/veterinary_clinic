﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinarianClinic
{
    public class Dog : Animal
    {
        private bool doesItBite;

        public bool DoesItBite { get => doesItBite; set => doesItBite = value; }

        public Dog() { }
        public Dog(string name, string owner, float size, float weight, bool doesItBite) : base(name, owner, size, weight)
        {
            this.doesItBite = doesItBite;
        }


    }
}
