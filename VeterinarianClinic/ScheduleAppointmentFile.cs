﻿using System.IO;
using System.Xml.Serialization;

namespace VeterinarianClinic
{
    public class ScheduleAppointmentFile
    {
        private string dir = @"schedules";
        private string filePath = @"schedules\schedules.xml";

        public bool InitialDirectorySetUp()
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            if (!File.Exists(filePath))
            {
                var file = File.Create(filePath);
                file.Close();

                XmlSerializer serializer = new XmlSerializer(typeof(AppointmentsList));
                TextWriter writer = null;

                try
                {
                    writer = new StreamWriter(filePath);
                    serializer.Serialize(writer, new AppointmentsList());
                }
                catch (IOException)
                {
                    return false;
                }
                finally
                {
                    writer.Close();
                }
            }

            return true;
        }

        public bool SetSchedule(AppointmentsList appointmentsList)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(AppointmentsList));
            TextWriter writer = null;
            try
            {
                writer = new StreamWriter(filePath);
                serializer.Serialize(writer, appointmentsList);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
            finally
            {
                writer.Close();
            }
        }

        public AppointmentsList GetSchedules()
        {
            AppointmentsList appointmentsList = null;
            StreamReader reader = null;
            if (File.Exists(filePath))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(AppointmentsList));
                    reader = new StreamReader(filePath);
                    appointmentsList = (AppointmentsList)serializer.Deserialize(reader);
                }
                catch (IOException)
                {
                    return new AppointmentsList();
                }
                finally
                {
                    reader.Close();
                }
            }
            return appointmentsList;
        }
    }
}
