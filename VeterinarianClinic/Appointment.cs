﻿using System;

namespace VeterinarianClinic
{
    public class Appointment : IDisposable
    {
        private string appointmentTimeString;
        private bool isIndoor;
        private ServicesProvided service;
        private AnimalType type;
        private bool specificBehavior;
        private Animal animal;

        public Appointment()
        {

        }

        public string AppointmentTimeString { get => appointmentTimeString; set => appointmentTimeString = value; }
        public bool IsIndoor { get => isIndoor; set => isIndoor = value; }
        public ServicesProvided Service { get => service; set => service = value; }
        public Animal Animal { get => animal; set => animal = value; }
        public AnimalType Type { get => type; set => type = value; }
        public bool SpecificBehavior { get => specificBehavior; set => specificBehavior = value; }
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool ValidateTime()
        {
            return appointmentTimeString != null && appointmentTimeString != string.Empty;
        }
    }
}
