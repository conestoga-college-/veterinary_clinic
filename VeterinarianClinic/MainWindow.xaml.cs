﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace VeterinarianClinic
{
    public partial class MainWindow : Window
    {
        private AnimalTypeClass animalTypeClass = null;
        private AppointmentsList appointmentsList = null;
        private ScheduleAppointmentFile scheduleAppointmentFile = null;
        private bool isLocal = true;
        MyAnimal myAnimal = new MyAnimal();
        public AppointmentsList AppointmentsList { get => appointmentsList; set => appointmentsList = value; }

        public MyAnimal MAnimal { get => myAnimal; set => myAnimal = value; }

        public MainWindow()
        {
            InitializeComponent();
            appointmentsList = new AppointmentsList();
            DataContext = this;
            scheduleAppointmentFile = new ScheduleAppointmentFile();
            scheduleAppointmentFile.InitialDirectorySetUp();
            SetComboBoxScheduleTimes();

            Services services = new Services();
            cmbServices.ItemsSource = services.GetServicesStrings();
            cmbServices.SelectedIndex = 0;

            animalTypeClass = new AnimalTypeClass();
            cmbAnimals.ItemsSource = animalTypeClass.GetAllAnimalTypes();
            cmbAnimals.SelectedIndex = 0;

            btnUpdate.IsEnabled = false;
            btnDelete.IsEnabled = false;
            btnLoad.IsEnabled = true;
            btnSearch.IsEnabled = false;
            btnSave.IsEnabled = false;
        }

        private void SetComboBoxScheduleTimes()
        {
            List<string> scheduledTimes = GetAppointmentTimesFromFile();
            List<string> appointments = GetAllScheduleTimesExist();

            for (int i = 0; i < scheduledTimes.Count; i++)
            {
                appointments.Remove(scheduledTimes[i]);
            }

            cmbSlots.ItemsSource = appointments;
        }

        private List<string> GetAppointmentTimesFromFile()
        {
            AppointmentsList listAppointments = scheduleAppointmentFile.GetSchedules();

            List<string> scheduledTimes = new List<string>();
            if (listAppointments != null)
            {
                foreach (Appointment appointment in listAppointments.Appointments)
                {
                    scheduledTimes.Add(appointment.AppointmentTimeString);
                }
            }
            return scheduledTimes;
        }

        private List<string> GetAllScheduleTimesExist()
        {
            AppointmentTimeString appointmentTime = new AppointmentTimeString();
            List<string> appointments = appointmentTime.GetAppointmentsStrings();
            if (appointmentsList.Appointments.Count > 0)
            {
                for (int i = 0; i < appointmentsList.Appointments.Count; i++)
                {
                    appointments.Remove(appointmentsList.Appointments[i].AppointmentTimeString);
                }
            }
            return appointments;
        }

        private void UpdateTimeComboBox()
        {
            List<string> scheduledTimes = GetAppointmentTimesFromFile();
            List<string> appointments = GetAllScheduleTimesExist();
            if (cmbSlots.SelectedIndex > -1)
            {
                for (int j = 0; j < scheduledTimes.Count(); j++)
                {
                    appointments.Remove(scheduledTimes[j]);
                }
            }
            cmbSlots.ItemsSource = appointments;
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = ValidateInput();
            string name = string.Empty;
            string owner = string.Empty;
            float size = 0;
            float weight = 0;

            if (!isValid)
            {
                lblError.Visibility = Visibility.Visible;
                isValid = false;
            }
            if (cmbSlots.SelectedIndex <= -1)
            {
                lblErrorSlots.Visibility = Visibility.Visible;
                isValid = false;
            }

            if (isValid)
            {
                name = txtName.Text;
                owner = txtOwner.Text;
                size = float.Parse(txtSize.Text);
                weight = float.Parse(txtWeight.Text);

                bool specificBehavior = ckbSpecificBehavior.IsChecked.GetValueOrDefault();

                using (Appointment a = new Appointment())
                {
                    a.AppointmentTimeString = cmbSlots.SelectedItem.ToString();
                    a.IsIndoor = ckbIsDoor.IsChecked.GetValueOrDefault();
                    a.Service = (ServicesProvided)Enum.Parse(typeof(ServicesProvided), cmbServices.SelectedValue.ToString());

                    Animal animal = null;

                    lblError.Visibility = Visibility.Hidden;
                    lblErrorSlots.Visibility = Visibility.Hidden;
                    switch (cmbAnimals.SelectedIndex)
                    {
                        case (int)AnimalType.Cat:
                            animal = new Cat(name, owner, size, weight, specificBehavior);
                            break;
                        case (int)AnimalType.Dog:
                            animal = new Dog(name, owner, size, weight, specificBehavior);
                            break;
                        case (int)AnimalType.Horse:
                            animal = new Horse(name, owner, size, weight, specificBehavior);
                            break;
                        default:
                            MessageBox.Show("Invalid data");
                            break;
                    }

                    a.Animal = animal;
                    a.Type = (AnimalType)Enum.Parse(typeof(AnimalType), cmbAnimals.SelectedValue.ToString());
                    a.SpecificBehavior = specificBehavior;

                    appointmentsList.Appointments.Add(a);

                    if (grdAppointment.ItemsSource != appointmentsList.Appointments)
                    {
                        grdAppointment.ItemsSource = appointmentsList.Appointments;
                    }
                }

                UpdateTimeComboBox();
                ResetForm();
                btnSave.IsEnabled = true;
                cmbServices.IsEnabled = true;
                cmbAnimals.IsEnabled = true;
                btnLoad.IsEnabled = false;
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            scheduleAppointmentFile = new ScheduleAppointmentFile();

            bool isSuccess = scheduleAppointmentFile.SetSchedule(appointmentsList);
            isSuccess = scheduleAppointmentFile.SetSchedule(appointmentsList);

            if (isSuccess)
            {
                MessageBox.Show("Save data into file successfully!", "Notification", MessageBoxButton.OK, MessageBoxImage.Information);
                btnLoad.IsEnabled = true;
                btnSearch.IsEnabled = true;
                btnAdd.IsEnabled = true;
                btnUpdate.IsEnabled = false;
                btnDelete.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Cannot save data into file. Please try again!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            cmbServices.IsEnabled = true;
            cmbAnimals.IsEnabled = true;
            UpdateTimeComboBox();
            ResetForm();
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            SetComboBoxScheduleTimes();
            scheduleAppointmentFile = new ScheduleAppointmentFile();
            appointmentsList = scheduleAppointmentFile.GetSchedules();

            if (appointmentsList != null)
            {
                isLocal = false;
                if (appointmentsList.Appointments.Count() > 0)
                {
                    if (grdAppointment.ItemsSource != appointmentsList.Appointments)
                    {
                        grdAppointment.ItemsSource = appointmentsList.Appointments;
                    }
                    btnSearch.IsEnabled = true;
                    isLocal = true;
                }
            }
            ResetForm();
            btnLoad.IsEnabled = false;
        }

        private void BtnSearch_Click(object sender, RoutedEventArgs e)
        {
            isLocal = false;
            string search = txtSearch.Text.ToLower();

            if (!string.IsNullOrEmpty(search))
            {
                var query = from a in appointmentsList.Appointments
                            where a.Type.ToString().ToLower().Contains(search)
                            select a;
                grdAppointment.ItemsSource = query;
            }
            else
            {
                grdAppointment.ItemsSource = appointmentsList.Appointments;
            }
            txtSearch.Text = string.Empty;
            isLocal = true;
        }

        public void ResetForm()
        {
            cmbAnimals.SelectedIndex = 0;
            cmbServices.SelectedIndex = 0;
            txtName.Text = string.Empty;
            txtOwner.Text = string.Empty;
            txtSize.Text = string.Empty;
            txtWeight.Text = string.Empty;
            ckbIsDoor.IsChecked = false;
            ckbSpecificBehavior.IsChecked = false;
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = ValidateInput();
            string name = string.Empty;
            string owner = string.Empty;
            float size = 0;
            float weight = 0;
            if (!isValid)
            {
                lblError.Visibility = Visibility.Visible;
                isValid = false;
            }
            else
            {
                lblError.Visibility = Visibility.Hidden;
                name = txtName.Text;
                owner = txtOwner.Text;
                size = float.Parse(txtSize.Text);
                weight = float.Parse(txtWeight.Text);
                bool specificBehavior = ckbSpecificBehavior.IsChecked.GetValueOrDefault();

                Appointment appointment = new Appointment();
                if (cmbSlots.SelectedIndex == -1)
                {
                    Appointment gridAppointment = grdAppointment.SelectedItem as Appointment;
                    appointment.AppointmentTimeString = gridAppointment.AppointmentTimeString;
                }
                else
                {
                    appointment.AppointmentTimeString = cmbSlots.SelectedValue.ToString();
                }
                appointment.IsIndoor = ckbIsDoor.IsChecked.GetValueOrDefault();
                appointment.Service = (ServicesProvided)Enum.Parse(typeof(ServicesProvided), cmbServices.SelectedValue.ToString());

                Animal animal = null;
                switch (cmbAnimals.SelectedIndex)
                {
                    case (int)AnimalType.Cat:
                        animal = new Cat(name, owner, size, weight, specificBehavior);
                        break;
                    case (int)AnimalType.Dog:
                        animal = new Dog(name, owner, size, weight, specificBehavior);
                        break;
                    case (int)AnimalType.Horse:
                        animal = new Horse(name, owner, size, weight, specificBehavior);
                        break;
                    default:
                        MessageBox.Show("Invalid data");
                        break;
                }
                appointment.Animal = animal;
                appointment.Type = (AnimalType)Enum.Parse(typeof(AnimalType), cmbAnimals.SelectedValue.ToString());
                appointment.SpecificBehavior = specificBehavior;
                int index = grdAppointment.SelectedIndex;
                isLocal = false;
                appointmentsList.Appointments.RemoveAt(index);
                appointmentsList.Appointments.Add(appointment);
                isLocal = true;

                if (cmbSlots.SelectedIndex > -1)
                {
                    UpdateTimeComboBox();
                }

                ResetForm();
                btnSave.IsEnabled = true;
                btnLoad.IsEnabled = false;
                cmbServices.IsEnabled = true;
                cmbAnimals.IsEnabled = true;
                btnUpdate.IsEnabled = false;
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            isLocal = false;
            if (grdAppointment.SelectedIndex != -1)
            {
                appointmentsList.Appointments.RemoveAt(grdAppointment.SelectedIndex);
                btnSave.IsEnabled = true;
                btnLoad.IsEnabled = false;
                isLocal = true;
                ResetForm();
            }
            if (appointmentsList.Appointments.Count == 0)
            {
                cmbAnimals.IsEnabled = true;
                cmbServices.IsEnabled = true;
                btnUpdate.IsEnabled = false;
                btnDelete.IsEnabled = false;
            }
        }

        private void GrdAppointment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (isLocal)
            {
                cmbServices.IsEnabled = false;
                cmbAnimals.IsEnabled = false;
                Appointment appointment = grdAppointment.SelectedItem as Appointment;
                cmbSlots.SelectedValue = appointment.AppointmentTimeString;
                cmbAnimals.SelectedValue = appointment.Type.ToString();
                cmbServices.SelectedValue = appointment.Service.ToString();
                ckbIsDoor.IsChecked = appointment.IsIndoor;
                ckbSpecificBehavior.IsChecked = appointment.SpecificBehavior;
                txtName.Text = appointment.Animal.Name;
                txtOwner.Text = appointment.Animal.OwnersName;
                txtSize.Text = appointment.Animal.Size.ToString();
                txtWeight.Text = appointment.Animal.Weight.ToString();

                btnUpdate.IsEnabled = true;
                btnDelete.IsEnabled = true;
                btnAdd.IsEnabled = false;
                btnSave.IsEnabled = true;
            }
            else
            {
                btnAdd.IsEnabled = true;
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            grdAppointment.Columns[0].Width = 60;
            grdAppointment.Columns[1].Width = 80;
            grdAppointment.Columns[2].Width = 70;
            grdAppointment.Columns[3].Width = 70;
            grdAppointment.Columns[4].Width = 70;
            grdAppointment.Columns[5].Width = 60;
            grdAppointment.Columns[6].Width = 60;
            grdAppointment.Columns[7].Width = 65;
            grdAppointment.Columns[8].Width = 110;
        }

        private void CmbAnimals_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cmbAnimals.SelectedIndex)
            {
                case (int)AnimalType.Cat:
                    lblSpecificBehavior.Content = "Does it scratch?";
                    break;
                case (int)AnimalType.Dog:
                    lblSpecificBehavior.Content = "Does it bite?";
                    break;
                case (int)AnimalType.Horse:
                    lblSpecificBehavior.Content = "Is domesticated?";
                    break;
                default:
                    MessageBox.Show("Invalid data");
                    break;
            }
        }

        public bool ValidateInput()
        {
            MyAnimal animal = new MyAnimal();

            if (!animal.ValidateName(txtName.Text))
            {
                return false;
            }
            if (!animal.ValidateOwnersName(txtOwner.Text))
            {
                return false;
            }
            if (!float.TryParse(txtSize.Text, out float size))
            {
                return false;
            }
            else
            {
                if (!animal.ValidateSize(size))
                {
                    return false;
                }
            }
            if (!float.TryParse(txtWeight.Text, out float weight))
            {
                return false;
            }
            else
            {
                if (!animal.ValidateWeight(weight))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
