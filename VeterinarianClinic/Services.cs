﻿using System;
using System.Collections.Generic;

namespace VeterinarianClinic
{
    public enum ServicesProvided
    {
        Therapeutic = 0,
        Surgery = 1,
        Vaccination = 2
    }

    public class Services
    {
        public Services()
        {

        }

        public List<string> GetServicesStrings()
        {
            Array services = Enum.GetValues(typeof(ServicesProvided));
            List<string> result = new List<string>();

            for (int i = 0; i < services.Length; i++)
            {
                ServicesProvided convertedService = (ServicesProvided)services.GetValue(i);
                result.Add(convertedService.ToString());
            }
            return result;
        }

        public string GetOneServiceString(int appointmentTimeId)
        {
            Array services = Enum.GetValues(typeof(AppointmentTime));
            int convertedService = (int)services.GetValue(appointmentTimeId);
            return convertedService.ToString();
        }
    }
}
