﻿using System;
using System.Collections.Generic;

namespace VeterinarianClinic
{
    public enum AnimalType
    {
        Cat,
        Dog,
        Horse
    }

    public class AnimalTypeClass
    {
        public AnimalTypeClass()
        {

        }

        public List<string> GetAllAnimalTypes()
        {
            Array animals = Enum.GetValues(typeof(AnimalType));
            List<string> result = new List<string>();

            for (int i = 0; i < animals.Length; i++)
            {
                AnimalType convertedAnimal = (AnimalType)animals.GetValue(i);
                result.Add(convertedAnimal.ToString());
            }
            return result;
        }

    }
}
