﻿using System.Text.RegularExpressions;

namespace VeterinarianClinic
{
    public abstract class Animal :IAnimal
    {
        private string name;
        private string ownersName;
        private float size;
        private float weight;

        private int maxNameLenght = 20;
        private int maxOwnersNameLenght = 20;
        private float maxSizeInCentimeters = 180.0f;
        private float minSizeInCentimeters = 20.0f;
        private float maxWeightInKilograms = 450.0f;
        private float minWeightInKilograms = 1.0f;

        public string Name { get => name; set => name = value; }
        public string OwnersName { get => ownersName; set => ownersName = value; }
        public float Size { get => size; set => size = value; }
        public float Weight { get => weight; set => weight = value; }

        public Animal() { }
        
        public Animal(string name, string owner, float size, float weight)
        {
            this.name = name;
            this.ownersName = owner;
            this.size = size;
            this.weight = weight;
        }

        public bool ValidateName(string value)
        {
            return value != null && value != string.Empty && value.Length <= maxNameLenght && Regex.IsMatch(value, @"^[a-zA-Z]+$");
        }

        public string StringOfNameRule()
        {
            return $"It can not be empty or more than {maxNameLenght} alphabet characters";
        }

        public bool ValidateOwnersName(string value)
        {           
            return value != null && value != string.Empty && value.Length <= maxOwnersNameLenght && Regex.IsMatch(value, @"^[a-zA-Z]+$"); 
        }

        public string StringOfOwnersNameRule()
        {
            return $"It can not be empty or more than {maxOwnersNameLenght} alphabet characters";
        }

        public bool ValidateSize(float value)
        {
            return value <= maxSizeInCentimeters && value >= minSizeInCentimeters;
        }

        public string StringOfSizeRule()
        {
            return $"It can not be higher than {maxSizeInCentimeters} or lower {minSizeInCentimeters} centimeters.";
        }

        public bool ValidateWeight(float value)
        {
            return value <= maxWeightInKilograms && value >= minWeightInKilograms;
        }

        public string StringOfWeightRule()
        {
            return $"It can not be heavier than {maxWeightInKilograms} or lighter {minWeightInKilograms} kilograms.";
        }
    }
}
