﻿using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace VeterinarianClinic
{
    [XmlRoot("AppointmentsList")]
    [XmlInclude(typeof(Dog))]
    [XmlInclude(typeof(Cat))]
    [XmlInclude(typeof(Horse))]
    public class AppointmentsList
    {
        private ObservableCollection<Appointment> appointments;

        public AppointmentsList()
        {
            appointments = new ObservableCollection<Appointment>();
        }

        [XmlArray("Appointments")]
        [XmlArrayItem("Appointment", typeof(Appointment))]
        public ObservableCollection<Appointment> Appointments { get => appointments; set => appointments = value; }

    }
}
