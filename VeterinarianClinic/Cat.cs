﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeterinarianClinic
{
    public class Cat : Animal
    {
        private bool doesItScratch;

        public bool DoesItScratch { get => doesItScratch; set => doesItScratch = value; }

        public Cat()
        {

        }
        public Cat(string name, string owner, float size, float weight, bool doesItScratch) : base(name,owner,size,weight)
        {
            this.doesItScratch = doesItScratch;
        } 
    }
}
